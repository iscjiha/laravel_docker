1. Create folder for project
2. Create into the folder 3 folders (mysql, nginx, src)
3. Create in root a file called docker-compose.yml
    - Set docker-compose version
    - Set services
        - nginx
        - mysql
        - php
    - Set network
4. Create nginx config file
    - fastcgi_pass (Port 9000 same as service php)
5. Create Dockerfile
    - Set PHP image to use compatible with laravel version (FROM)
    - Set extensions to use (RUN)
6. Install laravel clean in src/:
    - composer create-project laravel/laravel .

-----------------------------------------------------------------------

Docker commands

    * docker ps -a

Docker compose commands

    * docker-compose up -d (Built project)
    * docker-compose down (Down project)
    * docker-compose logs -f (Check logs)
    * docker-compose ps (Check processes)    
    * docker-compose excec php php /var/www/html/artisan migrate

Important

    Edit .env with variables used in docker-compose.yml

TODO

    - Check if i can update docker compose version on docker-compose.yml
    - How can i use exists database in docker
    - How versioning Laravel docker project